import './App.css';
import routes from './routes';
import { Toaster } from 'react-hot-toast';

const App = () => (
  <>
    <Toaster />
    {routes()}
  </>
);

export default App;
