function Loader() {
  return (
    <div id="loader" className="loader-overlay">
      <div className="loader"></div>
    </div>
  )
}

export default Loader

