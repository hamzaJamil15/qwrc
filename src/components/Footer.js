import wave from '../assets/wave.png'

function Footer() {
  return <div className="footer">
    <h1>
      <i className="fas fa-copyright"></i>
      {" "}
      Copyrights 
      are reserved by QWRC
    </h1>
  </div>;
}

export default Footer;
