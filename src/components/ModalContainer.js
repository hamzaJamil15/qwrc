import PropTypes from 'prop-types';
import Modal from 'react-responsive-modal';
import 'react-responsive-modal/styles.css';

function ModalContainer({ children, isModalOpen, closeModal, heading }) {
  return (
    // <div>
    <Modal
      open={isModalOpen}
      center
      classNames={{
        overlay: 'customOverlay',
        modal: 'customModal',
      }}
      onClose={closeModal}
    >
      <div className="modal-heading">
        {heading}
      </div>
      <hr />

      {children}

    </Modal>
    // </div>
  );
}

ModalContainer.propTypes = {
  isModalOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  heading: PropTypes.string.isRequired,
};

export default ModalContainer;
