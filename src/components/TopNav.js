import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Logo1 from '../assets/logo1.png';

function TopNav() {
  const [pathName, setPathName] = useState('');

  const renderDropDown = () => (
    <div className="topnav_dropdown dropdown">
      <button className="btn btn-secondary dropdown-toggle dropdown_btn" type="button" id="dropdownMenuButton1"
        data-bs-toggle="dropdown" aria-expanded="false">
        <i className="fas fa-bars"></i>
      </button>
      <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
        <Link to="/#"
          onClick={() => {
            window.scrollTo(0, 0)
          }}
        >
          <a className="dropdown-item" >
            Home
          </a>
        </Link>
        <Link to="#about"
          onClick={() => {
            const a = document.getElementById('about');
            a.scrollIntoView({ behavior: 'smooth' });
          }}
        >
          <a className="dropdown-item">
            About
          </a>
        </Link>
        <Link to="#contact"
          onClick={() => {
            const a = document.getElementById('contact');
            a.scrollIntoView({ behavior: 'smooth' });
          }}
        >
          <a className="dropdown-item">
            Contact
          </a>
        </Link>
      </ul>
    </div>
  )

  useEffect(() => {

    window.addEventListener('scroll', (e) => {
      const about = document.getElementById('about');
      const contact = document.getElementById('contact');

      if ((e.path[1].scrollY + 20) >= about.offsetTop && e.path[1].scrollY <= contact.offsetTop - 50)
        setPathName('about');
      else if (e.path[1].scrollY >= contact.offsetTop - 49)
        setPathName('contact');
      else
        setPathName('');
    })

    return () => {
      window.addEventListener('scroll', () => {
      })
    }
  }, [])

  return (
    <div className={`top_nav ${pathName ? 'top_nav_bg' : ''}`}>
      <div className="top_nav-nav container">
        <div className="navbar_logo">
          <Link
            className="navbar_brand"
            to="/#"
            onClick={() => {
              window.scrollTo(0, 0)
              setPathName(null);
            }}
          >
            <img src={Logo1} alt="logo" height="50" />
          </Link>
        </div>
        {renderDropDown()}
        <div className="navbar_row">
          <Link
            className={`${pathName ? '' : 'active'}`}
            to="/#"
            onClick={() => {
              window.scrollTo(0, 0)
              setPathName(null);
            }}
          >
            Home
          </Link>
          <Link
            className={`${pathName === 'about' ? 'active' : ''}`}
            to="#about"
            onClick={() => {
              const a = document.getElementById('about');
              a.scrollIntoView({ behavior: 'smooth' });
            }}
          >
            About
          </Link>

          <Link
            className={`${pathName === 'contact' ? 'active' : ''}`}
            to='#contact'
            onClick={() => {
              const a = document.getElementById('contact');
              a.scrollIntoView({ behavior: 'smooth' });
            }}
          >
            Contact
          </Link>
        </div>
      </div>

    </div>
  );
}

export default TopNav;
