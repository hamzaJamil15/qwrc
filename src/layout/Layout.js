import React from 'react';
import { Outlet } from 'react-router';
import TopNav from '../components/TopNav';
import Homepage from '../pages/Homepage';
import About from '../pages/About';
import Footer from '../components/Footer';
import Contact from '../pages/Contact';
import Loader from '../components/Loader';

function Layout() {
  return (
    <div>
      <TopNav />
      <Homepage />
      <About />
      <Contact />
      <Footer />
    </div>
  );
}

export default Layout;
