import PropTypes from 'prop-types';
import bgImg from '../assets/bgImg-1.jpg';

function Homepage() {
  return (
    <div className="homepage">
      <div className="homepage_heading">
        <h1>Plastic</h1>
        <div className="heading_change">
          <div className="heading_move">
            <h1>Recycling</h1>
            <h1>Coloring</h1>
            <h1>Melting</h1>
            <h1>Molding</h1>
          </div>
        </div>
      </div>
      <img src={bgImg} alt="background_Image" />
      <div className="overlay"></div>
    </div>
  );
}

Homepage.propTypes = {};

export default Homepage;
