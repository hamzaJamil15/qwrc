import { useState } from 'react';
import emailjs from 'emailjs-com';
import toast from 'react-hot-toast';

function Contact() {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [loading, setLoading] = useState(false);


  const onChangeHandler = (setState) => (e) => {
    setState(e.target.value)
  }

  const _submit = async (e) => {
    e.preventDefault();

    const user_id = "user_dFwXqJ7Iif6RLevi2LmEe";
    const service_id = "service_jci0m1a"
    const template_id = "template_u3f9wee";

    const emailData = {
      name: name,
      email: email,
      message: message
    }

    setLoading(true);
    await emailjs.send(service_id, template_id, emailData, user_id)
      .then((result) => {
        console.log(result.text);
        toast.success("Message send to QWRC team");
      }, (error) => {
        console.log(error.text);
        toast.error("Message not send successfully");
      });

    setName('');
    setEmail('');
    setMessage('');
    e.target.reset();
    setLoading(false);

  }

  const renderContactForm = () => (
    <form onSubmit={_submit}>

      <label>Name</label>
      <input className="form-control" type="text" name="name" onChange={onChangeHandler(setName)}
        placeholder="Enter your name" required />

      <label>Email</label>
      <input className="form-control" type="email" onChange={onChangeHandler(setEmail)}
        placeholder="Enter your email" required />

      <label>Message</label>
      <textarea className="form-control" name="message" id="" cols="30" rows="5" required
        onChange={onChangeHandler(setMessage)} />

      <button type="submit" className={`submit-btn ${loading ? 'submit_loader' : ''}`} disabled={loading}>
        {!loading ? "Send Message" : ""}
      </button>

    </form>
  )

  const renderAddress = () => (
    <div className="contact-address">
      <i class="fas fa-map-marker-alt"></i>
      <h1>Unit 11 Mission Hill, Bawtry Road, <br />
        Doncaster, South Yorkshire, <br />
        England, DN10 6BH</h1>
    </div>
  )

  return (
    <div id="contact" className="contact">
      {/* <Loader /> */}
      <div className="contact-header"></div>
      <div className="contact-wrapper">
        <div className="contact-heading"><h1>Contact Us</h1></div>
        <div className="contact-row">
          <div className="contact-form">{renderContactForm()}</div>
          {renderAddress()}
        </div>
      </div>
    </div>
  )
}

Contact.propTypes = {};

export default Contact;
