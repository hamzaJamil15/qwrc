import PropTypes from 'prop-types';
import { useState } from 'react';
import img1 from '../assets/scrap1.jpg';
import img2 from '../assets/scrap2.jpg';
import img3 from '../assets/scrap3.jpg';
import img4 from '../assets/scrap4.jpg';
import img5 from '../assets/scrap5.jpg';
import img6 from '../assets/scrap6.jpg';
import ModalContainer from '../components/ModalContainer';
import { plasticInfo } from '../lib/healper';

function About() {
  const [isOpen, setIsOpen] = useState(false);
  const [heading, setHeading] = useState('');
  const [count, setCount] = useState(null);

  const openModal = (title, index) => {
    setHeading(title);
    setCount(index);
    setIsOpen(true);
  }

  const renderAboutItem = (image, title, index) => (
    <div className="about-item">
      <img src={image} alt="scrap-1" />
      <h1>{title}</h1>
      <button onClick={() => { openModal(title, index) }} >More</button>
    </div>
  )

  return (
    <div id="about" className="about">

      <div className="about-header"></div>

      <div className="about-heading">
        <h2>About Us</h2>
      </div>

      <div className="about-row">
        {renderAboutItem(img1, 'We buy plastic scrap', 0)}
        {renderAboutItem(img2, 'Plastic regrind', 1)}
        {renderAboutItem(img3, 'Plastic melting', 2)}
      </div>


      <div className="about-row">
        {renderAboutItem(img4, 'Plastic coloring', 3)}
        {renderAboutItem(img5, 'Plastic molding', 4)}
        {renderAboutItem(img6, 'Plastic compounds', 5)}
      </div>

      <ModalContainer isModalOpen={isOpen} closeModal={() => { setIsOpen(false) }}
        heading={heading} >

        <div className="about-content">
          <p> {plasticInfo(count)} </p>
        </div>

      </ModalContainer>

    </div>
  );
}

About.propTypes = {};

export default About;
