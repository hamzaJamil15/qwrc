import { useNavigate } from 'react-router';
import Logo1 from '../assets/logo1.png';

function ErrorPage() {
  const navigate = useNavigate();

  return (
    <div className="error_page">
      <div className="text-center">
        <h2>404 ERROR PAGE NOT FOUND</h2>
        <button className="btn btn-primary" onClick={() => navigate(-1)}>
          Go Back
        </button>
      </div>
    </div>
  );
}

export default ErrorPage;
